# shellcheck disable=SC2162

questions=($'Enter package name: \n' $'Enter package version (1.0.0): \n' $'Enter package description: \n' $'Enter package license (unknown): \n' $'Add all files in directory to source? [Y/n]\n' $'Generate MD5 sums? [Y/n]\n' $'Choose a package preset [regular] [none]\n')
source=()
sums=()
files=false
folders=false
mainadded=false
addFilesToSource() {
    for f in ./*
    do
      if [[ $f != *.iml ]] && [[ $f != PKGBUILD ]] && [[ $f != package_temp ]] && [[ -f $f ]]
      then
        source+=( $f )
      fi
    done
}


#get pkgbuild info here

if [ -e ./PKGBUILD ]; then
  read -p $'This will overwrite your current PKGBUILD, are you sure? [y/N]\n' confirmation
  case $confirmation in
    [Yy])
      rm PKGBUILD
      ;;
    [Nn])
      exit
      ;;
    *)
      exit
      ;;
  esac
fi

if [ -e ./package_temp ]; then
  rm package_temp
fi

for question in "${questions[@]}"; do
   case $question in
    "${questions[0]}")
      read -p "${questions[0]}" pkgname
      ;;
    "${questions[1]}")
      read -p "${questions[1]}" pkgver
      pkgver=${pkgver:-1.0.0}
      if [[ ! $pkgver =~ ^[0-9].[0-9].[0-9]$ ]]; then
        echo $'Wrong PKGVER format, please re-enter: \n'
        read -p "${questions[1]}" pkgver
        pkgver=${pkgver:-1.0.0}
      fi
      ;;
    "${questions[2]}")
      read -p "${questions[2]}" pkgdesc
      ;;
    "${questions[3]}")
      read -p "${questions[3]}" license
      license=${license:-unknown}
      ;;
    "${questions[4]}")
      read -p "${questions[4]}" source_confirmation
      case $source_confirmation in
      [Nn])
        ;;
      *)
        addFilesToSource
        ;;
      esac
      ;;
    "${questions[5]}")
      read -p "${questions[5]}" md5_confirmation
      case $md5_confirmation in
      [Nn])
        ;;
      *)
        for f in ./*
        do
          if [[ $f != *.iml ]] && [[ $f != ./PKGBUILD ]] && [[ $f != ./package_temp ]] && [[ -f $f ]]
          then
            sum=$( md5sum $f | cut -d ' ' -f 1 )
            sums+=( $sum )
          fi
        done
        ;;
      esac
      ;;
    "${questions[6]}")
      read -p "${questions[6]}" preset_choice
      case $preset_choice in
        "regular")
          ;;
        "none")
          ;;
        *)
          echo "Not an option!"
          ;;
      esac
      ;;
    *)
      echo "Unknown question!"
      ;;
   esac
done

touch package_temp

#generate package preset directories
for f in ./*
do
  if [ -f $f ]; then
    if [ $files == false ]; then
      echo 'sudo mkdir -p "${pkgdir}/usr/bin"' >> package_temp
      files=true
    fi
  elif [ -d $f ]; then
    if [ $folders == false ]; then
      echo $"sudo mkdir '/usr/share/$pkgname'" >> package_temp
      folders=true
    fi
  fi
done


#if directories are done then generate copy commands
if [ $files = true ] && [ $folders = true ]; then
  for f in ./*
  do
    if [[ $f == ./main.sh ]] || [[ $f == ./main.bash ]] && [[ $mainadded = false ]]; then
      f="${f:1}"
      echo 'sudo cp "${srcdir}'$f'" "/usr/bin/'$pkgname'"' >> package_temp
      mainadded=true
    elif [[ $f != ./package_temp ]] && [[ ! $f == ./PKGBUILD ]] && [[ ! $f == *.iml ]] && [[ ! -d $f ]]; then
      f="${f:1}"
      echo 'sudo cp "${srcdir}'$f'" "/usr/share/'$pkgname'"' >> package_temp
    elif [[ $f != ./package_temp ]] || [[ $f != ./PKGBUILD ]] && [[ -d $f ]]; then
      f="${f:1}"
      echo 'sudo cp -r "${srcdir}/..'$f'" "/usr/share/'$pkgname'"' >> package_temp
    fi
  done
  echo 'sudo chmod +x /usr/bin/'$pkgname >> package_temp
fi



#generate pkgbuild here
touch PKGBUILD
echo "pkgname='$pkgname'" >> PKGBUILD
echo "pkgver='$pkgver'" >> PKGBUILD
echo "pkgrel='1'" >> PKGBUILD
echo "pkgdesc='$pkgdesc'" >> PKGBUILD
echo "arch=('x86_64')" >> PKGBUILD
echo "license=('$license')" >> PKGBUILD
echo "source=(${source[*]})" >> PKGBUILD
echo "md5sums=(${sums[*]})" >> PKGBUILD
if [ $preset_choice = "regular" ]
then
  info=$(cat package_temp)
  echo " " >> PKGBUILD
  echo "package() {
  $info
  }" >> PKGBUILD
fi

rm package_temp

echo "PKGBUILD has been created at $(pwd)"
echo "Make sure to check the resulting PKGBUILD for any errors!"